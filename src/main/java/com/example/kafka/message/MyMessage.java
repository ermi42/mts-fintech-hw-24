package com.example.kafka.message;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyMessage {
    private String id;
    private String content;
}



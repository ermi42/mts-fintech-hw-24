package com.example.kafka.controller;

import com.example.kafka.message.MyMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/kafka")
public class MessageController {
    private final KafkaTemplate<String, Object> kafkaTemplate;

    @PostMapping("/send")
    public void sendMessage(@RequestBody MyMessage message) {
        kafkaTemplate.send("topic", message);
    }
}



package com.example.kafka.producer;

import com.example.kafka.message.MyMessage;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class KafkaProducer {
    @Bean
    CommandLineRunner commandLineRunner(KafkaTemplate<String, Object> kafkaTemplate) {
        return args -> {
            kafkaTemplate.send("topic", new MyMessage("3456347Yd", "Custom message"));
        };
    }
}

